package com.qa.stepDefinitions;

import org.openqa.selenium.WebDriver;

import com.qa.pages.AccountCreation;
import com.qa.pages.LandingPage;
import com.qa.pages.SearchPage;
import com.qa.util.TestBase;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;

public class ShowDetailsSteps extends TestBase{
	LandingPage landingpage;
	SearchPage searchpage;
	WebDriver driver;
	
	@After
	public void closeBrowser() {
		quitBrowser();
	}
		
	@Given("^I am on the IMDb landing page$")
	public void navigateToLandingPage() {
	    driver = initialization();
	    landingpage = new LandingPage(driver);
	    searchpage = new SearchPage(driver);	 
	}
	
	@When("^I click sign in$")
	public void clickOn_SignIn() {
	    // Write code here that turns the phrase above into concrete actions
		landingpage.clickSignIn();
	}

	@When("^I click Menu option$")
	public void clickOn_Menu() {
	    // Write code here that turns the phrase above into concrete actions
		landingpage.clickMenu();    
	}

	@When("^I click on \"([^\"]*)\"$")
	public void clickOn_Link(String linkName) {
		landingpage.SelectTopRatedShows(linkName);    
	}

	@When("^I search for \"([^\"]*)\" show$")
	public void searchForShow(String showName) {
	    // Write code here that turns the phrase above into concrete actions
		searchpage.SearchBoxSendValue(showName);    
	}

	@When("^I click on the show \"([^\"]*)\"$")
	public void clickOn_Show(String showNameFromResult) {
	    // Write code here that turns the phrase above into concrete actions
		searchpage.cliclOn_EpisodeFromSuggestion(showNameFromResult);
	}
	
	@Then("^I expect \"([^\"]*)\" of an episode should be \"([^\"]*)\"$")
	public void verify_ShowDetails(String validateCondition, String expectedResult) {
	    // Write code here that turns the phrase above into concrete actions
		String actualRes = "";
	    if(validateCondition.equalsIgnoreCase("title"))
	    {
	    	actualRes = searchpage.getEpisodeTitle();	    	
	    }
	    else if(validateCondition.equalsIgnoreCase("rating"))
	    {
	    	actualRes = searchpage.getEpisodeRating();	    	
	    }
	    else if(validateCondition.equalsIgnoreCase("number of reviews"))
	    {
	    	actualRes = searchpage.getEpisodeReviews();
	    }
	    Assert.assertEquals(expectedResult, actualRes);
	}
}
