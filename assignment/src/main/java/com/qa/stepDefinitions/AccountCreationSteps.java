package com.qa.stepDefinitions;

import com.qa.pages.AccountCreation;
import com.qa.pages.HomePage;
import com.qa.util.TestBase;

import cucumber.api.java.After;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AccountCreationSteps extends TestBase{

	AccountCreation account;
	HomePage homepage;
	
	@After
	public void closeBrowser() {
		quitBrowser();
	}
	
	@When("^I click on create a new account$")
	public void user_clicks_on_create_a_new_account() {
	    // Write code here that turns the phrase above into concrete actions
	    account = new AccountCreation(driver);
	    account.clickOn_ProceedToCreateAccount();
	}

	@When("^I enter \"([^\"]*)\" in name field$")
	public void user_enters_in_name_field(String arg1) {
	    // Write code here that turns the phrase above into concrete actions
	account.enter_Name(arg1);    
	}
	
	@When("^I enter email in email field for user \"([^\"]*)\"$")
	public void create_new_email_and_enters_in_email_field_for_user(String arg1) {
	    // Write code here that turns the phrase above into concrete actions
	    account.create_Enter_Email(arg1);
	}
	
	@When("^I enter \"([^\"]*)\" in password field$")
	public void user_enters_in_password_field(String arg1) {
	    // Write code here that turns the phrase above into concrete actions
	    account.enter_Password(arg1);
	}

	@When("^I enter \"([^\"]*)\" in confirm password field$")
	public void user_enters_in_confirm_password_field(String arg1) {
	    // Write code here that turns the phrase above into concrete actions
	    account.enter_ConfirmPassword(arg1);
	}

	@When("^I click on create imdb account$")
	public void user_clicks_on_create_imdb_account() {
	    // Write code here that turns the phrase above into concrete actions
	    account.clickOn_CreateAccount();
	}

	@Then("^I expect user \"([^\"]*)\" to be login in to the application$")
	public void user_expects_to_login_to_the_application(String arg1) {
	    // Write code here that turns the phrase above into concrete actions
	    homepage = new HomePage(driver);
	    homepage.VerifyLogin(arg1);
	}
}
