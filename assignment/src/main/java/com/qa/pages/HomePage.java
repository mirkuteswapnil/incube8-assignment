package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import junit.framework.Assert;

public class HomePage {

	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	 }
	
	@FindBy(how = How.XPATH, using = "//div[@class=\"ipc-button__text\"]/span") 
	 WebElement lbl_SignedInUser;
	
	public void VerifyLogin(String arg1) {
		String actualSignInUser = lbl_SignedInUser.getText();
	Assert.assertEquals("Login Successful", arg1, actualSignInUser);
	}
}
