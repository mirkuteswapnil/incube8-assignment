package com.qa.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;


public class SearchPage {
	WebDriver driver;
	
	@FindBy(how=How.ID,using = "suggestion-search")
	WebElement serchBox;

	@FindBy(how=How.ID,using = "suggestion-search-button")
	WebElement searchButton;
	
	@FindBy(how=How.XPATH,using = "//ul[@class=\"react-autosuggest__suggestions-list anim-enter-done\"]/li")
	List<WebElement> suggestionListOfEpisodes;

	@FindBy(how=How.XPATH,using = "//*[@id='nav-search-form']/div[2]/div/div/div/ul")
	List<WebElement> searchResultLink1;
	
	@FindBy(how=How.XPATH,using = "//div[contains(@class,\"title_wrapper\")]/h1")
	WebElement showTitle;

	@FindBy(how=How.XPATH,using = "//div[contains(@class,'imdbRating')]//span[contains(@itemprop,'ratingValue')]")
	WebElement showRating;

	@FindBy(how=How.XPATH,using = "//span[@class='small' and @itemprop='ratingCount']")
	WebElement showReviews;

	public SearchPage(WebDriver driver) {
	PageFactory.initElements(driver, this);

	}
	public void SearchBoxSendValue(String searchName) {
	serchBox.sendKeys(searchName);
	//searchButton.click();
	}

	public void cliclOn_EpisodeFromSuggestion(String episodeNameToBeClicked) {	
	//searchResultLink.click();
	  //List < WebElement > mylist=driver.findElements(By.xpath());
	  //System.out.println(mylist.size());

	  for (int i = 0; i < suggestionListOfEpisodes.size(); i++) {
		    if (suggestionListOfEpisodes.get(i).getText().contains("Game of Thrones: The Last Watch")) {
		        //System.out.println(suggestionListOfEpisodes.get(i).getText());
		        //WebElement ele = driver.findElement(By.xpath(mylist.get(i)));
		        suggestionListOfEpisodes.get(i).click();
		        break;
		    }
	  }
	}
	
	public String getEpisodeTitle() {
		String pageTitle =  showTitle.getText();
		return pageTitle;
	}

	public String getEpisodeRating() {
		String showRate =  showRating.getText();
		return showRate;
	}

	public String getEpisodeReviews() {
		String episodeReview =  showReviews.getText();
		return episodeReview;
	}
}
