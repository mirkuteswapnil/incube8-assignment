package com.qa.pages;

import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AccountCreation {
	 
	public AccountCreation(WebDriver driver) {
		PageFactory.initElements(driver, this);
	 }
	
	@FindBy(how = How.LINK_TEXT, using = "Create a New Account") 
	 WebElement lnk_CreateAccount;
	
	@FindBy(how = How.ID, using = "ap_customer_name") 
	 WebElement txtbx_Name;
	
	@FindBy(how = How.ID, using = "ap_email") 
	 WebElement txtbx_Email;

	@FindBy(how = How.ID, using = "ap_password") 
	 WebElement txtbx_Password;

	@FindBy(how = How.ID, using = "ap_password_check") 
	 WebElement txtbx_ConfirmPassword;
	
	@FindBy(how = How.ID, using = "continue") 
	 WebElement btn_CreateAccount;
	
	public void clickOn_ProceedToCreateAccount() {
		lnk_CreateAccount.click();
	}
	
	public void enter_Name(String name) {
		txtbx_Name.sendKeys(name);
	}
	
	public void create_Enter_Email(String name){
		Date date = new Date();
	    long timeMilli = date.getTime(); //This method returns the time in millis
	    String newEmail = name + timeMilli+"@gmail.com";
	    txtbx_Email.sendKeys(newEmail);
	}
	
	public void enter_Email(String email) {
		txtbx_Email.sendKeys(email);
	}
	
	public void enter_Password(String password) {
		txtbx_Password.sendKeys(password);
	}

	public void enter_ConfirmPassword(String confirmPassword) {
		txtbx_ConfirmPassword.sendKeys(confirmPassword);
	}

	public void clickOn_CreateAccount() {
		btn_CreateAccount.click();
	}
}
