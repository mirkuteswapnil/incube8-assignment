package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.TestBase;

public class LandingPage{
		
	@FindBy(how=How.ID,using = "imdbHeader-navDrawerOpen--desktop")
	WebElement menuO;
	
	@FindBy(how=How.LINK_TEXT,using = "Menu")
	WebElement menu;

	@FindBy(how=How.LINK_TEXT,using = "Top Rated Shows")
	WebElement showLink;

	@FindBy(how=How.LINK_TEXT,using = "Sign In")
	WebElement signInLink;

	public LandingPage(WebDriver driver) {
	PageFactory.initElements(driver, this);
	}

	public void clickMenu() {
	menuO.click();
	}

	public void SelectTopRatedShows(String linkName) {
		if(linkName.equalsIgnoreCase("Top Rated Shows"))
			showLink.click();
	}
	
	public void clickSignIn(){
		signInLink.click();	
	}
}
