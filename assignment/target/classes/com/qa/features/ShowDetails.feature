Feature: Verify top rated tv show details

Scenario: Verification of an Episode
Given I am on the IMDb landing page
When I click Menu option 
And I click on "Top Rated Shows"
And I search for "Game of Thrones" show
And I click on the show "Game of Thrones: The Last Watch"
Then I expect "title" of an episode should be "Game of Thrones: The Last Watch (2019)"
And I expect "rating" of an episode should be "7.2"
And I expect "number of reviews" of an episode should be "5,258"