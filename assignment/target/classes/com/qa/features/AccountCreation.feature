Feature: Create new account and verify login for newly registered users

Scenario Outline: Create an account and verify login
Given I am on the IMDb landing page
When I click sign in
And I click on create a new account
And I enter "<name>" in name field
And I enter email in email field for user "<name>"
And I enter "<password>" in password field
And I enter "<password>" in confirm password field
And I click on create imdb account
Then I expect user "<name>" to be login in to the application

Examples:
| name    |password    |
| Swapnil |Password123 | 
| Indrasen|Password123 |