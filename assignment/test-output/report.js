$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/com/qa/features/AccountCreation.feature");
formatter.feature({
  "name": "Create new account and verify login for newly registered users",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Create an account and verify login",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I am on the IMDb landing page",
  "keyword": "Given "
});
formatter.step({
  "name": "I click sign in",
  "keyword": "When "
});
formatter.step({
  "name": "I click on create a new account",
  "keyword": "And "
});
formatter.step({
  "name": "I enter \"\u003cname\u003e\" in name field",
  "keyword": "And "
});
formatter.step({
  "name": "I enter email in email field for user \"\u003cname\u003e\"",
  "keyword": "And "
});
formatter.step({
  "name": "I enter \"\u003cpassword\u003e\" in password field",
  "keyword": "And "
});
formatter.step({
  "name": "I enter \"\u003cpassword\u003e\" in confirm password field",
  "keyword": "And "
});
formatter.step({
  "name": "I click on create imdb account",
  "keyword": "And "
});
formatter.step({
  "name": "I expect user \"\u003cname\u003e\" to be login in to the application",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "name",
        "password"
      ]
    },
    {
      "cells": [
        "Swapnil",
        "Password123"
      ]
    },
    {
      "cells": [
        "Indrasen",
        "Password123"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Create an account and verify login",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I am on the IMDb landing page",
  "keyword": "Given "
});
formatter.match({
  "location": "ShowDetailsSteps.navigateToLandingPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click sign in",
  "keyword": "When "
});
formatter.match({
  "location": "ShowDetailsSteps.clickOn_SignIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on create a new account",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_clicks_on_create_a_new_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"Swapnil\" in name field",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_enters_in_name_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter email in email field for user \"Swapnil\"",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.create_new_email_and_enters_in_email_field_for_user(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"Password123\" in password field",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_enters_in_password_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"Password123\" in confirm password field",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_enters_in_confirm_password_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on create imdb account",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_clicks_on_create_imdb_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect user \"Swapnil\" to be login in to the application",
  "keyword": "Then "
});
formatter.match({
  "location": "AccountCreationSteps.user_expects_to_login_to_the_application(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Create an account and verify login",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "I am on the IMDb landing page",
  "keyword": "Given "
});
formatter.match({
  "location": "ShowDetailsSteps.navigateToLandingPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click sign in",
  "keyword": "When "
});
formatter.match({
  "location": "ShowDetailsSteps.clickOn_SignIn()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on create a new account",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_clicks_on_create_a_new_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"Indrasen\" in name field",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_enters_in_name_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter email in email field for user \"Indrasen\"",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.create_new_email_and_enters_in_email_field_for_user(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"Password123\" in password field",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_enters_in_password_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I enter \"Password123\" in confirm password field",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_enters_in_confirm_password_field(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on create imdb account",
  "keyword": "And "
});
formatter.match({
  "location": "AccountCreationSteps.user_clicks_on_create_imdb_account()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect user \"Indrasen\" to be login in to the application",
  "keyword": "Then "
});
formatter.match({
  "location": "AccountCreationSteps.user_expects_to_login_to_the_application(String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("src/main/java/com/qa/features/ShowDetails.feature");
formatter.feature({
  "name": "Verify top rated tv show details",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verification of an Episode",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I am on the IMDb landing page",
  "keyword": "Given "
});
formatter.match({
  "location": "ShowDetailsSteps.navigateToLandingPage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click Menu option",
  "keyword": "When "
});
formatter.match({
  "location": "ShowDetailsSteps.clickOn_Menu()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on \"Top Rated Shows\"",
  "keyword": "And "
});
formatter.match({
  "location": "ShowDetailsSteps.clickOn_Link(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I search for \"Game of Thrones\" show",
  "keyword": "And "
});
formatter.match({
  "location": "ShowDetailsSteps.searchForShow(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on the show \"Game of Thrones: The Last Watch\"",
  "keyword": "And "
});
formatter.match({
  "location": "ShowDetailsSteps.clickOn_Show(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect \"title\" of an episode should be \"Game of Thrones: The Last Watch (2019)\"",
  "keyword": "Then "
});
formatter.match({
  "location": "ShowDetailsSteps.verify_ShowDetails(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect \"rating\" of an episode should be \"7.2\"",
  "keyword": "And "
});
formatter.match({
  "location": "ShowDetailsSteps.verify_ShowDetails(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I expect \"number of reviews\" of an episode should be \"5,258\"",
  "keyword": "And "
});
formatter.match({
  "location": "ShowDetailsSteps.verify_ShowDetails(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});