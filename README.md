# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : This is a technical test completed for incube-8. It comprises automation scenarios along with manual test cases for IMDb application.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up : Eclipse, Java version 11
* Configuration : Maven project
* Dependencies : Selenium Webdriver (version 3.141.0), Serenity with Cucumber-JVM (version 1.9.51)
* How to run tests : From Eclipse, run as JUnit test (TestRunner.java)
* Deployment instructions : Clone the repo. Relative paths have been used in the project.

### Contribution guidelines ###

* Writing tests : Manual Test Cases are written in TestCases.xlsx
